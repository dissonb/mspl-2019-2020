Membres :
	-   Zakari Yaou Abdou Mahamadou
    -   Bruno Disson


DataSet : 
	Jeu de données du trafic annuel entrant par station de métro pour l'année 2018 : https://dataratp.opendatasoft.com/explore/dataset/trafic-annuel-entrant-par-station-du-reseau-ferre-2018/information/

Questions posées :
	- Le revenu moyen des habitants influe-t-il sur le trafic du métro ? (non retenue, trop de paramètres extérieurs peuvent influer sur le résultat)
	- Existe-t-il un impact sur l'affluence à une station lors de la mise en service de lignes nouvelles (métro, bus, tramway) passant par cette station.
	- La nature des quartiers influe-t-elle sur le trafic dans les stations du métro présentes au sein de ce quartier ? (nature du quartier -> affaires, commercial, résidentiel, culturel...)

Question retenue :
	- Existe-t-il un impact sur l'affluence à une station lors de la mise en service de lignes nouvelles (métro, bus, tramway) passant par cette station.

Remarques :
	- Sur le dataSet donné, une personne entrante est décrite comme "toute personne entrant dans la station en ayant validée son ticket.
	  Les correspondances, ainsi que les personnes sortant de la station ne sont pas comptées.
	- Le DataSet comporte toutes les stations ferroviaires gérées par la RATP, ce qui explique la présence des stations de RER gérées par la RATP.
	  Les stations RER gérées par la SNCF ne sont pas présentes.


