The goal is to compare the popularity of the first names "Jean" and "Louis".

To do this, I made the choice to try to represent the data as a graph containing the following caracteristics

absciss -> date
ordinate left -> percentage of people being born named "Louis"
ordinate right -> percentage of people being born named "Jean"
curve 1 -> evolution of first name "Louis" in percentage in function of time
curve 2 -> evolution of first name "Jean" in percentage in function of time

The choice to use percentage instead of brut values is due to WW1 & 2

The R code is inspired by Luis W.